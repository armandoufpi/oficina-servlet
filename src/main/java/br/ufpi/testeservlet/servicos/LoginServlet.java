package br.ufpi.testeservlet.servicos;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String location="formulario-login.jsp";
		response.sendRedirect(location);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email="";
		String senha="";
		String location="principal";
		String mensagem="";
		
		email = request.getParameter("email");
		senha = request.getParameter("senha");
		
		System.out.println("Faz o tratamento dos dados enviados pelo formulário login");
		if (email.equals("armando@ufpi.edu.br") && senha.equals("123")){
			mensagem = "Acesso realizado com sucesso!";
			request.setAttribute("email", email);
			request.getRequestDispatcher(location).forward(request, response);
		}else{
			mensagem = "Erro nas credenciais usuário/senha!";
			response.sendRedirect(location);
		}
	}

}