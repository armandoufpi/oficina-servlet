package br.ufpi.testeservlet.servicos;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PrincipalServlet
 */
public class PrincipalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PrincipalServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//se não tem acesso ao sistema redireciona o usuário para o formulario de login
		String location="formulario-login.jsp";
		response.sendRedirect(location);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//acessa o sistema e mostra a página principal do sistema
		String location="pagina-principal.jsp";
		
		//Como o servlet principal recebe parâmetros (e-mail e senha) de outro servlet (LoginServlet) é preciso fazer um reencaminhamento interno dentro da própria aplicacão
		request.getRequestDispatcher(location).forward(request, response);
	}

}
